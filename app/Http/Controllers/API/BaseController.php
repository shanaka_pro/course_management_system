<?php


namespace App\Http\Controllers\API;


/**
 * @OA\Info(title="Course Management System API Doc",
 * version="1.0", description="Course Management System API description",
 * @OA\Contact(email="shanakavpm@gmail.com" ))

 *
 * @OAS\SecurityScheme(
 *      securityScheme="bearer_token",
 *      type="http",
 *      scheme="bearer",
 *      security={{"bearer_token":{}}},
 * )
 */




use App\Http\Controllers\Controller as Controller;



class BaseController extends Controller
{


    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($result, $message)
    {
        $response = [
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];


        return response()->json($response, 200);
    }


    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $error,
        ];


        if (!empty($errorMessages)) {
            $response['data'] = $errorMessages;
        }


        return response()->json($response, $code);
    }
}
